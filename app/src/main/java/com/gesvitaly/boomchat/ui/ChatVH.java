package com.gesvitaly.boomchat.ui;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.gesvitaly.boomchat.R;

import me.himanshusoni.chatmessageview.ChatMessageView;

public class ChatVH extends RecyclerView.ViewHolder {
    ChatMessageView chatMessageView;
    TextView messageText;
    TextView messageUser;
    TextView messageTime;

    public ChatVH(View itemView) {
        super(itemView);
        chatMessageView = (ChatMessageView) itemView.findViewById(R.id.chatMessageView);
        messageText = (TextView) itemView.findViewById(R.id.message_text);
        messageUser = (TextView) itemView.findViewById(R.id.message_user);
        messageTime = (TextView) itemView.findViewById(R.id.message_time);
    }
}
