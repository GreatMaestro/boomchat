package com.gesvitaly.boomchat.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.gesvitaly.boomchat.R;
import com.gesvitaly.boomchat.model.ChatMessage;
import com.gesvitaly.boomchat.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    private static final int SIGN_IN_REQUEST_CODE = 666;
    private ChatFirebaseAdapter firebaseAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        } catch (DatabaseException ex) {}

        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            // Start sign in/sign up activity
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setProviders(Arrays.asList(new AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build(),
                                    new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build()))
                            .setIsSmartLockEnabled(false)
                            .build(),
                    SIGN_IN_REQUEST_CODE
            );
        } else initChat();
    }

    private User getUser() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            // User is signed in
            String displayName = user.getDisplayName();
            Uri profileUri = user.getPhotoUrl();
            String email = user.getEmail();
            String uid = user.getUid();

            // If the above were null, iterate the provider data
            // and set with the first non null data
            for (UserInfo userInfo : user.getProviderData()) {
                if (displayName == null && userInfo.getDisplayName() != null) {
                    displayName = userInfo.getDisplayName();
                }
                if (profileUri == null && userInfo.getPhotoUrl() != null) {
                    profileUri = userInfo.getPhotoUrl();
                }
                if (email == null && userInfo.getEmail() != null) {
                    email = userInfo.getEmail();
                }
            }
            return new User(uid, displayName, email, profileUri);
        }
        return new User(null, null, null, null);
    }

    private void initChat() {
        //FirebaseMessaging.getInstance().subscribeToTopic("baseRoom");
        final User user = getUser();
        // User is already signed in. Therefore, display
        // a welcome Toast
        Toast.makeText(this,
                "Welcome, " + (user.getName() != null ? user.getName() : user.getEmail()),
                Toast.LENGTH_LONG)
                .show();

        // Load chat room contents
        displayChatMessages(user.getUid());

        ImageButton fab = (ImageButton) findViewById(R.id.send_button);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText input = (EditText)findViewById(R.id.input);
                String text = input.getText().toString();
                if (text.trim().isEmpty()) return;

                // Read the input field and push a new instance
                // of ChatMessage to the Firebase database
                FirebaseDatabase.getInstance()
                        .getReference()
                        .child("baseRoom")
                        .push()
                        .setValue(new ChatMessage(text, user)
                        );

                // Clear the input
                input.setText("");

                    /*RemoteMessage message = new RemoteMessage.Builder(FirebaseInstanceId.getInstance().getToken())
                            .setMessageId("2")
                            .addData(user.getName(), "text")
                            .build();
                    FirebaseMessaging.getInstance().send(message);*/
            }
        });
    }

    private void displayChatMessages(String currentUserUid) {
        final RecyclerView messagesRV = (RecyclerView) findViewById(R.id.list_of_messages);
        firebaseAdapter = new ChatFirebaseAdapter(this, currentUserUid, ChatMessage.class, R.layout.message, ChatVH.class, FirebaseDatabase.getInstance().getReference().child("baseRoom"));
        messagesRV.setAdapter(firebaseAdapter);
        final LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setStackFromEnd(true);
        messagesRV.setLayoutManager(mLinearLayoutManager);

        firebaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                int friendlyMessageCount = firebaseAdapter.getItemCount();
                int lastVisiblePosition = mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
                if (lastVisiblePosition == -1 ||
                        (positionStart >= (friendlyMessageCount - 1) &&
                                lastVisiblePosition == (positionStart - 1))) {
                    messagesRV.scrollToPosition(positionStart);
                }
            }
        });

        final FloatingActionButton scrollToBottomFab = (FloatingActionButton) findViewById(R.id.scrollToBottomFab);
        messagesRV.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (mLinearLayoutManager.findLastCompletelyVisibleItemPosition() < firebaseAdapter.getItemCount() - 5) {
                    if (scrollToBottomFab.getVisibility() == View.GONE) {
                        scrollToBottomFab.show();
                    }
                } else if (scrollToBottomFab.getVisibility() == View.VISIBLE) scrollToBottomFab.hide();
            }
        });
        scrollToBottomFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLinearLayoutManager.scrollToPosition(firebaseAdapter.getItemCount() - 1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SIGN_IN_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                initChat();
            } else {
                Toast.makeText(this,
                        "Вход не удался. Попробуйте позже",
                        Toast.LENGTH_LONG)
                        .show();
                finish();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.menu_sign_out) {
            AuthUI.getInstance().signOut(this)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Toast.makeText(MainActivity.this,
                                    "Вы успешно вышли",
                                    Toast.LENGTH_LONG)
                                    .show();

                            // Close activity
                            finish();
                        }
                    });
        }
        return true;
    }
}
