package com.gesvitaly.boomchat.ui;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.gesvitaly.boomchat.R;
import com.gesvitaly.boomchat.model.ChatMessage;
import com.gesvitaly.boomchat.utils.DateFormatter;
import com.google.firebase.database.Query;

import java.util.Objects;

import cn.nekocode.badge.BadgeDrawable;

public class ChatFirebaseAdapter extends FirebaseRecyclerAdapter<ChatMessage, ChatVH> {
    private static final int MINE = 492;
    private static final int ANOTHER = 207;
    private String currentUserUid;
    private Context context;

    /**
     * @param currentUserUid
     * @param modelClass      Firebase will marshall the data at a location into
     *                        an instance of a class that you provide
     * @param modelLayout     This is the layout used to represent a single item in the list.
*                        You will be responsible for populating an instance of the corresponding
*                        view with the data from an instance of modelClass.
     * @param viewHolderClass The class that hold references to all sub-views in an instance modelLayout.
     * @param ref             The Firebase location to watch for data changes. Can also be a slice of a location,
*                        using some combination of {@code limit()}, {@code startAt()}, and {@code endAt()}.
     */
    public ChatFirebaseAdapter(Context context, String currentUserUid, Class<ChatMessage> modelClass, int modelLayout, Class<ChatVH> viewHolderClass, Query ref) {
        super(modelClass, modelLayout, viewHolderClass, ref);
        this.currentUserUid = currentUserUid;
        this.context = context;
    }

    @Override
    protected void populateViewHolder(final ChatVH chatVH, ChatMessage model, int position) {
        chatVH.messageText.setText(model.getText());

        String name = model.getUser().getName();
        if ("Виталий Гесь".equals(name)) {
            final BadgeDrawable adminLabel =
                    new BadgeDrawable.Builder()
                            .type(BadgeDrawable.TYPE_ONLY_ONE_TEXT)
                            .text1("Admin")
                            .build();
            SpannableString adminName =
                    new SpannableString(TextUtils.concat(name, " ", adminLabel.toSpannable()));
            chatVH.messageUser.setText(adminName);
        } else {
            String email = model.getUser().getEmail();
            chatVH.messageUser.setText(name != null ? name : email);
        }

        String formattedDate = DateFormatter.format(model.getTime(), DateFormatter.Template.TIME);
//        String formattedDate = DateFormat.format("dd-MM-yyyy (HH:mm:ss)", model.getTime());
        chatVH.messageTime.setText(formattedDate);

        chatVH.chatMessageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context != null) {
                    String text = String.valueOf(chatVH.messageText.getText());
                    ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("", text);
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(context, "Текст скопирован в буфер обмена", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        ChatMessage message = getItem(position);
        if (Objects.equals(currentUserUid, message.getUser().getUid())) return MINE;
        else return ANOTHER;
    }

    @Override
    public ChatVH onCreateViewHolder(ViewGroup parent, int viewType) {
        @LayoutRes int layoutId;
        switch (viewType) {
            case MINE: {
                layoutId = R.layout.message_right;
                break;
            }
            case ANOTHER: {
                layoutId = R.layout.message_left;
                break;
            }
            default: {
                layoutId = R.layout.message_left;
            }
        }
        View itemView = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
        return new ChatVH(itemView);
    }
}
