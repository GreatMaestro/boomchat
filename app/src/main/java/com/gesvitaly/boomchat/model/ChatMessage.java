package com.gesvitaly.boomchat.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class ChatMessage implements Parcelable {
    private String text;
    private User user;
    private long time;

    public ChatMessage(String text, User user) {
        this.text = text;
        this.user = user;

        // Initialize to current time
        time = new Date().getTime();
    }

    public ChatMessage(){

    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.text);
        dest.writeParcelable(this.user, flags);
        dest.writeLong(this.time);
    }

    protected ChatMessage(Parcel in) {
        this.text = in.readString();
        this.user = in.readParcelable(User.class.getClassLoader());
        this.time = in.readLong();
    }

    public static final Creator<ChatMessage> CREATOR = new Creator<ChatMessage>() {
        @Override
        public ChatMessage createFromParcel(Parcel source) {
            return new ChatMessage(source);
        }

        @Override
        public ChatMessage[] newArray(int size) {
            return new ChatMessage[size];
        }
    };
}
